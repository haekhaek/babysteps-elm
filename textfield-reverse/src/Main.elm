module Main exposing (..)

import Browser
import Html exposing (Html, Attribute, div, text, input)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)

main =
    Browser.sandbox { init = init, update = update, view = view }

type alias Model =
    { content: String
    }

init: Model
init =
    { content = "Hello World2!"}

type Msg =
    Change String

update: Msg -> Model -> Model
update myMsg model =
    case myMsg of
        Change myNewString ->
            { model | content = myNewString }

view: Model -> Html Msg
view model =
    div []
        [ input [ placeholder "Text to reverse", value model.content, onInput Change ] []
        , div   [] [ reverseAndCount model |> text ]
        ]

reverseAndCount: Model -> String
reverseAndCount model = (String.reverse model.content) ++ " character count:  " ++ (String.length model.content |> String.fromInt)
