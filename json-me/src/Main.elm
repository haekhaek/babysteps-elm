module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode exposing (Decoder, field, string)

-- Main

main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

-- Model

type Model
    = Failure
    | Loading
    | Success String

init : () -> (Model, Cmd Msg)
init _ =
    ( Loading, getCoolCatGif)

-- Update

type Msg
    = MorePlease
    | GotGif (Result Http.Error String)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        MorePlease ->
            (Loading, getCoolCatGif)

        GotGif resultCat ->
            case resultCat of
                Err _ ->
                    (model, Cmd.none)
                Ok url ->
                    (Success url, Cmd.none)

-- Subscription

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none

-- View

view : Model -> Html Msg
view model =
    div []
        [ h2 [] [text "Cute Cats"]
        , viewGif model
        ]

viewGif : Model -> Html Msg
viewGif model =
    case model of
        Failure ->
            div []
                [ text "Sorry couldn't get More cat to display."
                , button [ onClick MorePlease ] [ text "Please more cats." ]
                ]
        Loading ->
            div []
                [ text "Cats are Loading..."
                ]
        Success url ->
            div []
                [ button [ onClick MorePlease, style "display" "block"] [ text "Please more cats."]
                , img [ src url ] []
                ]

-- Http

getCoolCatGif : Cmd Msg
getCoolCatGif =
    Http.get
        { url = "https://api.giphy.com/v1/gifs/random?api_key=aLFCZ5Eulk7iZrym4mHDSmyCdwveG8KP&tag=cat"
        , expect = Http.expectJson GotGif gifDecoder
        }

gifDecoder : Decoder String
gifDecoder =
    field "data" <| field "images" <| field "original" <| field "url" string
