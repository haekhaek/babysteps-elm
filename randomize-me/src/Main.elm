module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Events exposing (..)
import Random


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


-- MODEL


type alias Model =
    { dieFace : Int
    }

init : () -> (Model, Cmd Msg)
init _ =
    ( Model 1
    , Cmd.none
    )


-- UPDATE


type Msg
    = Roll
    | NewFace Int

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Roll ->
            ( model
            , Random.generate NewFace (Random.int 1 6)
            )
        NewFace myNumber ->
            ( Model myNumber
            , Cmd.none
            )


-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text (String.fromInt model.dieFace) ]
        , button [ onClick Roll ] [ text "generate new number" ]
        ]
