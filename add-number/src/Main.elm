module Main exposing (..)

import Browser
import Html exposing (div, text, input, button)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (class)
import String exposing (fromInt, toInt)
import Debug exposing (log)


type Messages = Add | ChangedAddText String

init : { value : number, firstName : String, numberToAdd : number }
init =
    {
        value = 56
        , firstName = "haekhaek"
        , numberToAdd = 0
    }

view : { a | value : Int } -> Html.Html Messages
view model =
    div [] [
         text (fromInt model.value)
        , div [] []
        , input [ onInput ChangedAddText ] []
        , button [ class "button", onClick Add ] [ text "Add" ]
        ]

update msg model =
    let
        _ = log "model" model
        _ = log "msg" msg
    in

    case msg of
        Add ->
            { model | value = model.value + model.numberToAdd }
        ChangedAddText myText ->
            { model | numberToAdd = (parseUserNumber myText) }

parseUserNumber : String -> Int
parseUserNumber myText =
    let
        theMaybe = toInt myText
    in
    case theMaybe of
        Just val -> val
        Nothing -> 0

main : Program () { value : Int, firstName : String, numberToAdd : Int } Messages
main =
    Browser.sandbox
        {
            init = init
            , view = view
            , update = update
        }
