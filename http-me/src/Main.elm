module Main exposing (..)

import Browser
import Html exposing (Html, text, pre, div)
import Http

main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

type Model
    = Success String
    | Failure
    | Loading


init : () -> (Model, Cmd Msg)
init _ =
    ( Loading
    , Http.get
          { url = "https://elm-lang.org/assets/public-opinion.txt"
          , expect = Http.expectString GotText
          }
    )

-- UPDATE
type Msg
    = GotText (Result Http.Error String)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        GotText result ->
            case result of
                Ok fullText ->
                    (Success fullText, Cmd.none)
                Err bar ->
                    (Failure, Cmd.none)

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

-- VIEW
view : Model -> Html Msg
view model =
    case model of
        Failure ->
            text "I was not able to load your book"
        Loading ->
            text "Loading..."
        Success book ->
            div [] [ pre [] [text book] ]
