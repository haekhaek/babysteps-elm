module Main exposing (..)
import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)

main =
    Browser.sandbox { init = init, update = update, view = view }

type alias Model =
    { name: String
    , password: String
    , passwordAgain: String
    }

type Msg
    = Name String
    | Password String
    | PasswordAgain String

init =
    Model "" "" ""

update : Msg -> Model -> Model
update msg model =
    case msg of
        Name name ->
            { model | name = name }
        Password password ->
            { model | password = password }
        PasswordAgain passwordAgain ->
            { model | passwordAgain = passwordAgain }

view : Model -> Html Msg
view model =
    div []
      [
        viewInput "text" "Name" model.name Name
      , viewInput "text" "Password" model.password Password
      , viewInput "text" "PasswordAgain" model.passwordAgain PasswordAgain
      , viewValidation model
      ]

viewInput : String -> String -> String -> (String -> Msg) -> Html Msg
viewInput t p v toMsg =
    input [ type_ t, placeholder p, value v, onInput toMsg ] []

viewValidation : Model -> Html msg
viewValidation model =
    if checkEqual model && checkIslong model && checkIsUpper model && checkIsNumber model then
        passwordOk
    else
        passwordBad "Password don't match"

checkIslong : Model -> Bool
checkIslong model = (String.length model.password) >= 8

checkIsUpper : Model -> Bool
checkIsUpper model = String.any Char.isUpper model.password

checkIsNumber : Model -> Bool
checkIsNumber model = String.any Char.isDigit model.password

checkEqual : Model -> Bool
checkEqual model = model.password == model.passwordAgain

passwordOk : Html msg
passwordOk =
    div [ style "color" "green"] [ text "OK" ]

passwordBad : String -> Html msg
passwordBad hint =
    div [ style "color" "red" ] [ text hint ]
